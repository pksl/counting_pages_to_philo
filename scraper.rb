require 'open-uri'
require 'nokogiri'

word = "/wiki/Droit"
base_url = "https://fr.wikipedia.org"
enter_url = "#{word}"
count = 0

until word == "/wiki/Philosophie"
  base_url = "https://fr.wikipedia.org"
  enter_url = "#{word}"
  page = Nokogiri::HTML(open(base_url + enter_url))
  rows = page.css('div.mw-content-ltr p a').attr('href')
  puts "Parsing..."
  p rows.value
  word = rows.value
  redo if rows.value.gsub(/[:]/)
  count +=1
  puts "#{count} -- #{enter_url}"
  sleep(2)
end
